import React from 'react';
import { useTheme } from '@mui/material';
import useMediaQuery from '@mui/material/useMediaQuery';

const useCustomMediaQuery = () => {
	const theme = useTheme();

	const isMobile: boolean = useMediaQuery(theme.breakpoints.between('xs', 'sm'));
	const isTablet: boolean = useMediaQuery(theme.breakpoints.between('sm', 'lg'));
	const isFullScreen: boolean = useMediaQuery(theme.breakpoints.up("lg"));

	return {
		isMobile,
		isTablet,
		isFullScreen,
	};
};

export default useCustomMediaQuery;
