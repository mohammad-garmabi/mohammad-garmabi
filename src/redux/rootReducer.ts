import { connectRouter } from 'connected-react-router';
import { combineReducers } from 'redux';
import { History } from 'history';

const rootReducer = (history: History<string>) => {
	return combineReducers({
		router: connectRouter<string>(history),
	});
};

export default rootReducer;
