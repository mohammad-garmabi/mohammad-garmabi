import React from 'react';
import asyncComponent from 'utils/asyncComponent';
import { RouteComponentProps, Switch, Route } from 'react-router-dom';

const HomeRoot: React.FC<RouteComponentProps> = props => {
	return (
		<Switch>
			<Route
				path={`${props.match.path}`}
				exact
				component={asyncComponent(() => import('./Page/index'))}
			/>
		</Switch>
	);
};

export default HomeRoot;
