import React from 'react';
import MobileUI from './Component/mobile';
import useCustomMediaQuery from 'hooks/useMediaQuery';
import { Box, Container, Grid, Paper, Theme } from '@mui/material';
import { useTheme } from '@mui/styles';

const HomePage = (props: any) => {
	const { isFullScreen, isMobile, isTablet } = useCustomMediaQuery();
	const theme: Theme = useTheme();

	const ParentCustomaize: React.ElementType = ({ children }: Element) => {
		if (!isMobile) {
			return <Container maxWidth={'lg'}>{children}</Container>;
		}

		return <>{children}</>;
	};

	return (
		<ParentCustomaize>
			<Box>
				<Box>{isFullScreen && 'fullScreen , Please resize me!'}</Box>

				<Box>{isTablet && 'tablet size and resize me!'}</Box>

				<Box>{isMobile && <MobileUI />}</Box>
			</Box>
		</ParentCustomaize>
	);
};

export default HomePage;
