import React from 'react';
import { ButtonKit } from 'components/kit/Button';

const Kit = () => (
	<ButtonKit color="success" variant="contained">
		Hello World
	</ButtonKit>
);

export default Kit;
