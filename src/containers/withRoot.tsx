import React from 'react';
import { create } from 'jss';
import rtl from 'jss-rtl';
import i18n from '../i18n/i18n';
import { CssBaseline } from '@mui/material';
import { faIR, enUS } from '@mui/material/locale';
import { themeOptions } from '../theme/default';
import { createTheme, responsiveFontSizes } from '@mui/material/styles';
import { jssPreset, StylesProvider, ThemeProvider } from '@mui/styles';
import jMoment from 'moment-jalaali';

export const IsRtl: React.FC<{ disableRtl?: boolean }> = props => {
	const plugins = [...jssPreset().plugins];
	if (!props.disableRtl) {
		plugins.push(rtl());
	}
	return <StylesProvider jss={create({ plugins })}>{props.children}</StylesProvider>;
};

function withRoot(Component: React.FC) {
	function WithTheme(props: any) {
		let locale = enUS;

		if (i18n.language.includes('fa')) {
			locale = faIR;
			jMoment.loadPersian({ dialect: 'persian-modern', usePersianDigits: true });
		}

		const theme = responsiveFontSizes(
			createTheme(
				{
					...themeOptions,
					direction: i18n.dir(),
				},
				locale,
			),
		);

		return (
			<ThemeProvider theme={theme}>
				<IsRtl>
					<CssBaseline />
					<Component {...props} />
				</IsRtl>
			</ThemeProvider>
		);
	}

	return WithTheme;
}

export default withRoot;
