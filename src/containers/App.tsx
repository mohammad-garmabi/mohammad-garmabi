import { Switch, Route, Redirect } from 'react-router-dom';
import HomeRoot from '../app/HomeRoot';
import asyncComponent from 'utils/asyncComponent';

const App = () => {
	return (
		<Switch>
			<Route path={`/kit`} exact component={asyncComponent(() => import('../app/Kit/kit'))} />
			<Route path={'/'} exact component={HomeRoot} />
			<Redirect from="*" to="/" />
		</Switch>
	);
};

export default App;
