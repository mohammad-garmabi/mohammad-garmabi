import { alpha, darken, lighten } from '@mui/material/styles';
import { Fade, ThemeOptions, Zoom } from '@mui/material';

// const fontSize = 10; // px
// // Tell Material-UI what's the font-size on the html element.
// // 16px is the default font-size used by browsers.
// const htmlFontSize = 10;
// const coef = fontSize / 10;

const themeOptions: ThemeOptions = {
	palette: {
		mode: 'dark',
		primary: {
			main: '#2A2A62',
			contrastText: '#fff',
			100: '#F4F4F7',
			200: '#ECECF4',
			300: '#D9D9E8',
			400: '#B2B2D2',
			500: '#8C8CBB',
			600: '#6565A5',
			700: '#3F3F8E',
			800: '#3F4096',
			900: '#363475',
		},
		secondary: {
			main: '#E0BB00',
			contrastText: '#fff',
			100: '#FFFDF4',
			200: '#FFFBE9',
			300: '#FFF8D3',
			400: '#FFF0A7',
			500: '#FFE97B',
			600: '#FFE14F',
			700: '#FFDA23 ',
			800: '#FFF215',
			900: '#FFCC2A',
		},
		success: {
			main: '#5DA600',
			contrastText: '#fff',
			100: ' ',
			200: '',
			300: '#F8FDF2',
			400: '#E3F5CC',
			500: '#ACE266',
			600: '#90D933',
			700: '#74CF00 ',
		},
		success2: {
			main: '#119273',
			contrastText: '#fff',
			100: ' ',
			200: '',
			300: '#F3FBF9 ',
			400: '#D0EEE7',
			500: '#72CDB8',
			600: '#43BDA0',
			700: '#14AC88 ',
		},
		error: {
			main: '#BE3737',
			contrastText: '#fff',
			100: ' ',
			200: '',
			300: '#FEF7F7 ',
			400: '#F9DDDD',
			500: '#EC9999',
			600: '#E67777',
			700: '#E05555 ',
		},
		warning: {
			main: '#E48900',
			contrastText: '#fff',
			100: ' ',
			200: '',
			300: '#FFFAF4  ',
			400: '#FDECD3',
			500: '#F9C77B',
			600: '#F7B44F',
			700: '#F5A123 ',
		},
		info: {
			main: '#007EB4',
			contrastText: '#fff',
			100: ' ',
			200: '',
			300: '#F3FBFE   ',
			400: '#CEEDFB',
			500: '#6CCAF2',
			600: '#3BB8EE',
			700: '#0AA7EA ',
		},
		grey: {
			100: '#E8E9F0',
			200: '#F6F7F9',
			300: '#FFFFFF',
		},
		text: {
			primary: '#FFFFFF',
			secondary: '#83819B',
			disabled: '#9B9BC4',
			dark: '#25233D',
		},
		divider: '#ecedef',
		background: {
			paper: '#F6F7F9',
			default: '#D7DBDF',
			light: '#FFFFFF',
			dark: '#262626',
		},
		common: {
			white: '#fff',
			black: '#000',
		},
	},
	shadows: [
		'none',
		'0px 4px 8px rgba(97, 97, 97, 0.14), 0px 8px 16px rgba(97, 97, 97, 0.14)',
		'0px 8px 18px rgba(0, 0, 0, 0.28)',
		'0px 18px 33px rgba(166, 94, 75, 0.35)',
		'0 10px 50px 0 rgba(0, 0, 0, 0.1)',
		'0px 3px 5px -1px rgba(0,0,0,0.2),0px 5px 8px 0px rgba(0,0,0,0.14),0px 1px 14px 0px rgba(0,0,0,0.12)',
		'0px 3px 5px -1px rgba(0,0,0,0.2),0px 6px 10px 0px rgba(0,0,0,0.14),0px 1px 18px 0px rgba(0,0,0,0.12)',
		'0px 4px 5px -2px rgba(0,0,0,0.2),0px 7px 10px 1px rgba(0,0,0,0.14),0px 2px 16px 1px rgba(0,0,0,0.12)',
		'0px 5px 5px -3px rgba(0,0,0,0.2),0px 8px 10px 1px rgba(0,0,0,0.14),0px 3px 14px 2px rgba(0,0,0,0.12)',
		'0px 5px 6px -3px rgba(0,0,0,0.2),0px 9px 12px 1px rgba(0,0,0,0.14),0px 3px 16px 2px rgba(0,0,0,0.12)',
		'0px 6px 6px -3px rgba(0,0,0,0.2),0px 10px 14px 1px rgba(0,0,0,0.14),0px 4px 18px 3px rgba(0,0,0,0.12)',
		'0px 6px 7px -4px rgba(0,0,0,0.2),0px 11px 15px 1px rgba(0,0,0,0.14),0px 4px 20px 3px rgba(0,0,0,0.12)',
		'0px 7px 8px -4px rgba(0,0,0,0.2),0px 12px 17px 2px rgba(0,0,0,0.14),0px 5px 22px 4px rgba(0,0,0,0.12)',
		'0px 7px 8px -4px rgba(0,0,0,0.2),0px 13px 19px 2px rgba(0,0,0,0.14),0px 5px 24px 4px rgba(0,0,0,0.12)',
		'0px 7px 9px -4px rgba(0,0,0,0.2),0px 14px 21px 2px rgba(0,0,0,0.14),0px 5px 26px 4px rgba(0,0,0,0.12)',
		'0px 8px 9px -5px rgba(0,0,0,0.2),0px 15px 22px 2px rgba(0,0,0,0.14),0px 6px 28px 5px rgba(0,0,0,0.12)',
		'0px 8px 10px -5px rgba(0,0,0,0.2),0px 16px 24px 2px rgba(0,0,0,0.14),0px 6px 30px 5px rgba(0,0,0,0.12)',
		'0px 8px 11px -5px rgba(0,0,0,0.2),0px 17px 26px 2px rgba(0,0,0,0.14),0px 6px 32px 5px rgba(0,0,0,0.12)',
		'0px 9px 11px -5px rgba(0,0,0,0.2),0px 18px 28px 2px rgba(0,0,0,0.14),0px 7px 34px 6px rgba(0,0,0,0.12)',
		'0px 9px 12px -6px rgba(0,0,0,0.2),0px 19px 29px 2px rgba(0,0,0,0.14),0px 7px 36px 6px rgba(0,0,0,0.12)',
		'0px 10px 13px -6px rgba(0,0,0,0.2),0px 20px 31px 3px rgba(0,0,0,0.14),0px 8px 38px 7px rgba(0,0,0,0.12)',
		'0px 10px 13px -6px rgba(0,0,0,0.2),0px 21px 33px 3px rgba(0,0,0,0.14),0px 8px 40px 7px rgba(0,0,0,0.12)',
		'0px 10px 14px -6px rgba(0,0,0,0.2),0px 22px 35px 3px rgba(0,0,0,0.14),0px 8px 42px 7px rgba(0,0,0,0.12)',
		'0px 11px 14px -7px rgba(0,0,0,0.2),0px 23px 36px 3px rgba(0,0,0,0.14),0px 9px 44px 8px rgba(0,0,0,0.12)',
		'0px 11px 15px -7px rgba(0,0,0,0.2),0px 24px 38px 3px rgba(0,0,0,0.14),0px 9px 46px 8px rgba(0,0,0,0.12)',
	],
	typography: {
		htmlFontSize: 10,
		fontFamily: ['Yekan Bakh'].join(', '),
		fontSize: 16,
		fontWeightLight: 300,
		fontWeightRegular: 400,
		fontWeightMedium: 500,
		fontWeightBold: 700,
		h1: {},
		h2: {},
		h3: {},
		h4: {},
		h5: {},
		h6: {},
		subtitle1: {},
		subtitle2: {},
		body1: {},
		body2: {},
		button: {},
		caption: {},
		overline: {},
	},
	shape: {
		small: 4,
		borderRadius: 8,
		medium: 12,
		large: 16,
	},
	zIndex: {
		mobileStepper: 1000,
		speedDial: 1050,
		appBar: 1100,
		drawer: 1200,
		modal: 1300,
		snackbar: 1400,
		tooltip: 1500,
	},
	components: {
		MuiCssBaseline: {
			styleOverrides: {
				html: {
					fontSize: '62.5%',
					'@media (min-width:1200px) and (max-width:1300px)': {
						fontSize: '47%',
					},
					'@media (min-width:1300px) and (max-width:1510px)': {
						fontSize: '62.5%',
					},
					'@media (min-width:1510px) and (max-width:1600px)': {
						fontSize: '60%',
					},
				},
				a: {
					color: 'inherit',
					textDecoration: 'none',
				},
			},
		},
		MuiOutlinedInput: {
			styleOverrides: {
				root: {},
			},
		},
		MuiFormLabel: {
			styleOverrides: {
				root: {
					fontWeight: 500,
					color: '#000',
					'&$focused': {
						color: 'auto',
					},
				},
			},
		},
		MuiFormControlLabel: {
			styleOverrides: {
				root: {
					marginLeft: 0,
					marginRight: '0.6rem',
					marginBottom: '0.4rem !important',
				},
			},
		},
		MuiTabs: {
			styleOverrides: {
				root: {
					overflow: 'visible',
				},
				scroller: {
					'@media (min-width:1440px)': {
						overflow: 'visible !important',
					},
					'@media (max-width:1440px)': {
						backgroundColor: '#fff',
					},
				},
			},
		},
		MuiStepLabel: {
			styleOverrides: {
				label: {
					color: '#565a64',
					fontWeight: 'normal',
					'&.MuiStepLabel-active': {
						color: '#1a879f',
						fontWeight: 'bold',
					},
					'&.MuiStepLabel-completed': {
						color: '#565a64',
						fontWeight: 'normal',
					},
				},
			},
		},
		MuiTableContainer: {
			styleOverrides: {
				root: {
					'&.MuiPaper-root ': {
						boxShadow: 'unset',
						backgroundColor: 'transparent',
					},
				},
			},
		},
		MuiTableHead: {
			styleOverrides: {
				root: {
					backgroundColor: '#f7f7f8',
					borderRadius: 12,
				},
			},
		},
		MuiTableBody: {
			styleOverrides: {
				root: {
					backgroundColor: '#fff',
				},
			},
		},
		MuiTableCell: {
			styleOverrides: {
				root: {
					padding: 22,
					borderBottomColor: '#f3f4f5',
				},
				head: {
					color: '#565a64',
					borderBottomWidth: 0,
					lineHeight: 0.73,
					// '&:first-child': {
					// 	borderBottomLeftRadius: 12,
					// },
					// '&:last-child': {
					// 	borderBottomRightRadius: 12,
					// },
				},
				body: {
					fontWeight: 600,
				},
			},
		},
		MuiDialog: {
			styleOverrides: {
				paper: {
					borderRadius: 8,
				},
			},
		},
		MuiBackdrop: {
			styleOverrides: {
				root: {},
			},
		},
		MuiChip: {
			styleOverrides: {
				sizeSmall: {
					height: 28,
				},
				root: {},
			},
		},
		MuiSelect: {
			styleOverrides: {
				select: {
					display: 'flex',
					alignItems: 'center',
				},
				outlined: {
					borderRadius: 16,
				},
				// IconComponent: (props) => (
				// 	<KeyboardArrowDownRoundedIcon fontSize={'small'} color={'inherit'} {...props} />
				// ),
			},
		},
		MuiSkeleton: {
			styleOverrides: {
				root: {
					borderRadius: '5px',
				},
			},
		},
		MuiMenuItem: {
			styleOverrides: {
				root: {
					fontWeight: 500,
				},
			},
		},

		//
		MuiButton: {
			defaultProps: {
				disableElevation: false,
				disableRipple: false,
				disableTouchRipple: true,
				disableFocusRipple: true,
				fullWidth: false,
			},

			styleOverrides: {
				root: {
					borderRadius: '0.8rem',
					paddingTop: '0.6rem',
					paddingBottom: '0.6rem',
					// textTransform: 'capitalize',
					'&.Mui-disabled': {
						cursor: 'not-allowed',
						pointerEvents: 'none',
					},
				},

				// startIcon: {
				// 	marginLeft: '1rem',
				// },
				// endIcon: {
				// 	marginLeft: '1rem',
				// },

				sizeLarge: {
					paddingTop: '1.6rem',
					paddingBottom: '1.6rem',
				},
				sizeSmall: {
					paddingTop: '0.7rem',
					paddingBottom: '0.7rem',
				},
				outlined: {
					borderColor: '#ecedef',
				},
			},
		},
		MuiLink: {
			defaultProps: {
				underline: 'none',
				variant: 'body1',
				color: 'inherit',
			},
			styleOverrides: {
				root: {
					fontWeight: 500,
					transition: 'all .04s ease',
					backfaceVisibility: 'hidden',
					'&[active]': {
						color: '#FFDA23',
						fontWeight: 500,
					},
					'&:hover': {
						color: '#FFDA23',
						fontWeight: 500,
					},
					'&[disabled]': {
						color: '#7f838f',
						pointerEvents: 'none',
					},
				},
			},
		},
		MuiTextField: {
			defaultProps: {
				InputLabelProps: {
					shrink: true,
				},
				fullWidth: true,
				variant: 'outlined',
			},
		},
		MuiTooltip: {
			defaultProps: {
				TransitionComponent: Zoom,
			},
			styleOverrides: {
				arrow: {
					color: '#ffffff',
				},
				tooltip: {
					borderRadius: 8,
					backgroundColor: 'transparent',
				},
			},
		},
		MuiAlert: {
			styleOverrides: {
				root: {
					minHeight: '2rem',
				},
				action: {
					padding: '0',
				},
				icon: {
					padding: '0.5rem 0',
					marginRight: '0',
					marginLeft: '1.2rem',
				},
				message: {
					padding: '1rem',
				},
			},
		},
	},
	breakpoints: {
		values: {
			xs: 0,
			sm: 600,
			md: 900,
			lg: 1440,
			xl: 1536,
		},
	},
};
export { themeOptions };
