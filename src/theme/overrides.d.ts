import '@mui/material/styles/createPalette';
import '@mui/material/styles/shadows';
import '@mui/system/createTheme/shape';

declare module '@mui/material/styles/createPalette' {
	interface PaletteOptions {
		success2: PaletteColorOptions;
	}
	interface TypeText {
		dark: string;
	}
	interface TypeBackground {
		light: string;
		dark: string;
	}
}

declare module '@mui/system/createTheme/shape' {
	interface Shape {
		small: number;
		medium: number;
		large: number;
	}
}
